from datetime import timezone
from django.db import models
from django.db import models

class AuditoriaFecha(models.Model):
    f_creacion = models.DateTimeField(default=timezone)
    f_actualizar = models.DateTimeField(default=timezone)
    
    class Meta:
        abstract = True


class Productos(models.Model):
    titulo = models.CharField(max_length=255)
    fecha = models.CharField(max_length=255)
    valor = models.FloatField()
    estado = models.BooleanField(default=True)
    direccion =  models.CharField(max_length=255, default='no confirmada')
    
    # def __str__(self) :
    #     return "Productos : {} {} {}".format( self.nom_producto, self.valor)
