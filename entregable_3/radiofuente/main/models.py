from django.db import models

from django.contrib.auth.models import User  # Suponiendo que tienes una clase Persona

class Persona(models.Model):
    nombre_completo = models.CharField(max_length=100)
    fecha_nacimiento = models.DateField(null=True)
    

    def __str__(self):
        return f"{self.nombre_completo}"
    
class Usuario(Persona):
    email = models.EmailField(unique=True) 
    contraseña = models.CharField(max_length=128)
    cedula = models.CharField(max_length=10)  
    telefono = models.CharField(max_length=15)  
    fecha_registro = models.DateTimeField(auto_now_add=True, null=True)  
    token = models.CharField(max_length=255, blank=True, null=True) 

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username