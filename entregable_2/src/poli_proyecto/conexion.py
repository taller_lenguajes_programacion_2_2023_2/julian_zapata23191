import sqlite3
import json
import re

class Conexion:
    """Clase que maneja la conexión y operaciones con una base de datos SQLite."""

    def __init__(self) -> None:
        """Constructor de la clase Conexion.

        Inicializa la conexión a la base de datos SQLite y carga consultas SQL desde un archivo JSON.

        Attributes:
            __user (str): Usuario de la base de datos (no utilizado actualmente).
            __password (str): Contraseña de la base de datos (no utilizado actualmente).
            __puerto (int): Puerto de la base de datos (no utilizado actualmente).
            __url (str): URL de la base de datos (no utilizado actualmente).
            __nom_db (str): Nombre de la base de datos SQLite.
            __querys (dict): Diccionario que almacena consultas SQL cargadas desde un archivo JSON.
            __conex (sqlite3.Connection): Objeto de conexión a la base de datos SQLite.
            __cursor (sqlite3.Cursor): Objeto cursor para ejecutar consultas SQL en la base de datos.
        """
        self.__user = ""
        self.__password = ""
        self.__puerto = 0
        self.__url = ""
        self.__nom_db = "db_julianzapata.sqlite"
        self.__querys = self.obtener_json()
        self.__conex = sqlite3.connect(self.__nom_db)
        self.__cursor = self.__conex.cursor()

    def obtener_json(self):
        """Carga consultas SQL desde un archivo JSON y las devuelve como un diccionario.

        Returns:
            dict: Diccionario que contiene consultas SQL.
        """
        ruta = "./entregable_2/src/poli_proyecto/static/sql/querys.json"
        querys = {}
        with open(ruta, 'r') as file:
            querys = json.load(file)
        return querys

    def crear_tabla(self, nom_tabla="", datos_tbl=""):
        """Crea una tabla en la base de datos SQLite.

        Args:
            nom_tabla (str): Nombre de la tabla a crear.
            datos_tbl (str): Datos de la tabla en formato SQL.

        Returns:
            bool: True si la tabla se crea con éxito, False en caso contrario.
        """
        if nom_tabla != "":
            datos = self.__querys[datos_tbl]
            query = self.__querys["create_tb"].format(nom_tabla, datos)
            self.__cursor.execute(query)
            return True
        else:
            return False

    def insertar_datos(self, nom_tabla="", nom_columns="", datos_carga=""):
        """Inserta datos en una tabla de la base de datos SQLite.

        Args:
            nom_tabla (str): Nombre de la tabla donde se insertarán los datos.
            nom_columns (str): Columnas donde se insertarán los datos en formato SQL.
            datos_carga (str): Datos a insertar en formato SQL.

        Returns:
            bool: True si los datos se insertan con éxito, False en caso contrario.
        """
        if nom_tabla != "":
            columnas = self.__querys[nom_columns]
            query = self.__querys["insert"].format(nom_tabla, columnas, datos_carga)
            self.__cursor.execute(query)
            self.__conex.commit()
            return True
        else:
            return False

    def actualizar_datos(self, nom_tabla="", nom_columns="", datos_carga=""):
        """Actualiza datos en una tabla de la base de datos SQLite.

        Args:
            nom_tabla (str): Nombre de la tabla donde se actualizarán los datos.
            nom_columns (str): Columnas que se actualizarán en formato SQL.
            datos_carga (str): Nuevos datos en formato SQL.

        Returns:
            bool: True si los datos se actualizan con éxito, False en caso contrario.
        """
        if nom_tabla != "":
            columnas = self.__querys[nom_columns]
            query = self.__querys["update"].format(nom_tabla, columnas, datos_carga)
            self.__cursor.execute(query)
            self.__conex.commit()
            return True
        else:
            return False

    def borrar_datos(self, nom_tabla="", datos_carga=""):
        """Elimina datos de una tabla de la base de datos SQLite.

        Args:
            nom_tabla (str): Nombre de la tabla de donde se eliminarán los datos.
            nom_columns (str): Columnas que se utilizarán como criterio de eliminación en formato SQL.
            datos_carga (str): Datos que se utilizarán como criterio de eliminación en formato SQL.

        Returns:
            bool: True si los datos se eliminan con éxito, False en caso contrario.
        """
        if nom_tabla != "":
            query = self.__querys["delete"].format(nom_tabla, datos_carga)
            self.__cursor.execute(query)
            self.__conex.commit()
            return True
        else:
            return False
