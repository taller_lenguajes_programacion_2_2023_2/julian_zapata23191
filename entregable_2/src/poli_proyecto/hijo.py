# poo
# objeto
# atributos (caratcteristicas) y funciones (acciones)

# diagrama clases      +   diagrama objetos      +    programacion entidades
#
from conexion import Conexion
import pandas as pd


class Hijo(Conexion):
    
    def __init__(
        self, id=0, nombre="", telefono="", patineta="", bicicleta="", grado=""):
        """Constructor de la clase Hijo.

        Args:
            id (int, opcional): Identificador único del hijo. Por defecto, 0.
            nombre (str, opcional): Nombre del hijo. Por defecto, "".
            telefono (str, opcional): Número de teléfono del hijo. Por defecto, "".
            patineta (str, opcional): Nombre de la patineta del hijo. Por defecto, "".
            bicicleta (str, opcional): Nombre de la bicicleta del hijo. Por defecto, "".
            grado (str, opcional): Grado escolar del hijo. Por defecto, "".
        """
        self.__id = id
        self.nombre = nombre
        self.telefono = telefono
        self.patineta = patineta
        self.bicicleta = bicicleta
        self.grado = grado
        super().__init__()
        self.create_Hijo()
        ruta = "./entregable_2/src/poli_proyecto/static/xlsx/datos_proyecto.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="hijo")
        self.insert_hijo()
        self.select_hijo(id=0)
        #self.update_hijo()
        #self.delete_hijo()

    @property
    def id(self):
        """Obtiene el identificador único del hijo."""
        return self.__id

    @id.setter
    def id(self, value):
        """Establece el identificador único del hijo."""
        self.__id = value

    @property
    def _nombre(self):
        """Obtiene el nombre del hijo."""
        return self.nombre

    @_nombre.setter
    def _nombre(self, value):
        """Establece el nombre del hijo."""
        self.nombre = value

    @property
    def _telefono(self):
        """Obtiene el número de teléfono del hijo."""
        return self.telefono

    @_telefono.setter
    def _telefono(self, value):
        """Establece el número de teléfono del hijo."""
        self.telefono = value

    @property
    def _patineta(self):
        """Obtiene el nombre de la patineta del hijo."""
        return self.patineta

    @_patineta.setter
    def _patineta(self, value):
        """Establece el nombre de la patineta del hijo."""
        self.patineta = value

    @property
    def _bicicleta(self):
        """Obtiene el nombre de la bicicleta del hijo."""
        return self.bicicleta

    @_bicicleta.setter
    def _bicicleta(self, value):
        """Establece el nombre de la bicicleta del hijo."""
        self.bicicleta = value

    @property
    def _grado(self):
        """Obtiene el grado escolar del hijo."""
        return self.grado

    @_grado.setter
    def _grado(self, value):
        """Establece el grado escolar del hijo."""
        self.grado = value

        # Crear tabla Hijo
    def create_Hijo(self):
        """Crea la tabla 'hijos' en la base de datos."""
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="hijos",datos_tbl="datos_hijo"):
            print("Tabla Hijo Creada!!!")
        
        return atributos
    
    def insert_hijo(self):
        """Inserta registros en la tabla 'hijos'."""
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}"'.format(row["id"],row["nombre"],row["telefono"],row["patineta"],row["bicicleta"],row["grado"])
            self.insertar_datos(nom_tabla="hijos",nom_columns="carga_hijo",datos_carga=datos)
        return True
    
    def update_hijo(self,id=0):
        """Actualiza registros en la tabla 'hijos'."""
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}"'.format(id,row["nombre"],row["telefono"],row["patineta"],row["bicicleta"],row["grado"])
            self.actualizar_datos(nom_tabla="hijos",nom_columns="carga_hijo",datos_carga=datos)
        return True
    
    def delete_hijo(self,id=0):
        """Elimina registros de la tabla 'hijos'."""
        self.borrar_datos(nom_tabla="hijos",datos_carga=id)
        return True
    
    def select_hijo(self,id=[]):
        return Hijo
            
    def __str__(self) :
        """funcion para visualizar objeto hijo
        Args:
            no tiene parametros 
        """
        return "Padre : id {}  nombre {}".format(self.__id,self.nombre)

