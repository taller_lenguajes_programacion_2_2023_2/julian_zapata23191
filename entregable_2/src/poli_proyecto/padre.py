# poo
# objeto
# atributos (caratcteristicas) y funciones (acciones)

# diagrama clases      +   diagrama objetos      +    programacion entidades
#
from conexion import Conexion
import pandas as pd


class Padre(Conexion):
    
    def __init__(
        self, id=0, nombre="", telefono="", carro="", moto="", profesion=""):
        """Esta es la clase padre 
        Args:
            id (int, optional): identificador unico de la . Defaults to 0.
            nombre (str, optional): _description_. Defaults to "".
            telefono (str, optional): _description_. Defaults to "".
            carro (str, optional): _description_. Defaults to "".
            moto (str, optional): _description_. Defaults to "".
            profesion (str, optional): _description_. Defaults to "".
        """
        self.__id = id
        self.nombre = nombre
        self.telefono = telefono
        self.carro = carro
        self.moto = moto
        self.profesion = profesion
        super().__init__()
        self.create_Padre()
        ruta = "./entregable_2/src/poli_proyecto/static/xlsx/datos_proyecto.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="padre")
        self.insert_padre()
        self.select_Padre(id=0)
        #self.update_Padre()
        #self.delete_Padre()

    @property
    def id(self):
        """Obtiene el identificador único del padre."""
        return self.__id

    @id.setter
    def id(self, value):
        """Establece el identificador único del padre."""
        self.__id = value

    @property
    def _nombre(self):
        """Obtiene el nombre del padre."""
        return self.nombre

    @_nombre.setter
    def _nombre(self, value):
        """Establece el nombre del padre."""
        self.nombre = value

    @property
    def _telefono(self):
        """Obtiene el número de teléfono del padre."""
        return self.telefono

    @_telefono.setter
    def _telefono(self, value):
        """Establece el número de teléfono del padre."""
        self.telefono = value

    @property
    def _carro(self):
        """Obtiene el nombre del carro del padre."""
        return self.carro

    @_carro.setter
    def _carro(self, value):
        """Establece el nombre del carro del padre."""
        self.carro = value

    @property
    def _moto(self):
        """Obtiene el nombre de la moto del padre."""
        return self.moto

    @_moto.setter
    def _moto(self, value):
        """Establece el nombre de la moto del padre."""
        self.moto = value

    @property
    def _profesion(self):
        """Obtiene la profesión del padre."""
        return self.profesion

    @_profesion.setter
    def _profesion(self, value):
        """Establece la profesión del padre."""
        self.profesion = value

        # Crear tabla Padre
    def create_Padre(self):
        """Crea la tabla 'padres' en la base de datos."""
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="padres",datos_tbl="datos_padre"):
            print("Tabla Padre Creada!!!")
        
        return atributos
    
    def insert_padre(self):
        """Inserta registros en la tabla 'padres'."""
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}"'.format(row["id"],row["nombre"],row["telefono"],row["carro"],row["moto"],row["profesion"])
            self.insertar_datos(nom_tabla="padres",nom_columns="carga_padre",datos_carga=datos)
        return True
    
    def update_Padre(self,id=0):
        """Actualiza registros en la tabla 'padres'."""
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}"'.format(id, "Rodolfo",row["telefono"],row["carro"],row["moto"],row["profesion"])
            self.actualizar_datos(nom_tabla="padres",nom_columns="carga_padre",datos_carga=datos)
        return True
    
    def delete_Padre(self,id=0):
         """Elimina registros de la tabla 'padres'."""
         self.borrar_datos(nom_tabla="padres",datos_carga=id)
    
    def select_Padre(self,id=[]):
        return Padre
            
    def __str__(self) :
        """funcion para visualizar objeto padre
        Args:
            no tiene parametros 
        """
        return "Padre : id {}  nombre {}".format(self.__id,self.nombre)

