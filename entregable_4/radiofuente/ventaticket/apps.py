from django.apps import AppConfig


class VentaticketConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ventaticket'
