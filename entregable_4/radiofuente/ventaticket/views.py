from django.shortcuts import get_object_or_404, render

from .models import Productos

def lista(request):
    n_productos = Productos.objects.count()
    lista_prod = Productos.objects.all()
    return render(request,'home.html',{'n_prod':n_productos,'lista_p':lista_prod})


def detalle_producto(request,id):
    producto = get_object_or_404(Productos,pk=id)
    return render(request,'ticket-details.html',{'producto':producto})

def inicio(request):
    mensaje={ 'txt_inicio':"Hola Mundo"
    }
    return render(request,'main/inicio.html',mensaje)
