let signup = document.querySelector(".signup");
let login = document.querySelector(".login");
let slider = document.querySelector(".slider");
let formSection = document.querySelector(".form-section");
const expresionRegularCorreo = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
const telefonoRegex = /^\d+$/;
var correo = "";
var password = ";"


signup.addEventListener("click", () => {
	slider.classList.add("moveslider");
	formSection.classList.add("form-section-move")
});

login.addEventListener("click", () => {
	slider.classList.remove("moveslider");
	formSection.classList.remove("form-section-move");
});

function validateLogin() {
	let email = document.getElementById("loginEmail").value;
	let password = document.getElementById("loginEmail").value;

	email = email.trim();
	password = password.trim();

	let correo = localStorage.getItem('correo');
	let passwordStorage =  localStorage.getItem('password');

	if(email != correo && password != passwordStorage){
		alert("Ingrese credenciales válidas");
		email.value = "";
    	password.value = "";
		window.location.reload();
		return;
	}


	window.location.href = "./static/index.html";
}

function validateSignUp() {

	let nameRegister = document.getElementById("nameRegister").value;
	let emailRegister = document.getElementById("emailRegister").value;
	let passwordRegister = document.getElementById("passwordRegister").value;
	let confirmPasswordRegister = document.getElementById("confirmPasswordRegister").value;
	let phoneRegister = document.getElementById("phoneRegister").value;

	nameRegister = nameRegister.trim();
	emailRegister = emailRegister.trim();
	passwordRegister = passwordRegister.trim();
	confirmPasswordRegister = confirmPasswordRegister.trim();

	// VALIDA si todos los campos se digitaron
	if(!nameRegister || !emailRegister || !passwordRegister || !confirmPasswordRegister){
		alert("Ingrese todos los campos");
		return;
	}

	// VALIDA si el correo está bien escrito
	let correoValido = validarCorreo(emailRegister);
	if (!correoValido) {
		alert("El correo electrónico no es válido.");
		return;
	}

	let phoneValido = validarPhone(phoneRegister);
	if (!phoneValido) {
		alert("El telefono ingresado no es válido.");
		return;
	}

	// VALIDA si las dos contraseñas coinciden
	if(passwordRegister != confirmPasswordRegister){
		alert("Las contraseñas ingresadas NO coinciden.");
		return;
	}

		// Para guardar el correo electrónico
		localStorage.setItem('correo', emailRegister);

		// Para guardar la contraseña (NO se recomienda para contraseñas reales)
		localStorage.setItem('password', passwordRegister);

		localStorage.setItem('nombre', nameRegister);
	
		alert("Usuario registrado correctamente");
		window.location.reload();
}

function validarCorreo(correo) {
    return expresionRegularCorreo.test(correo);
}

function validarPhone(phone){
	return telefonoRegex.test(phone);
}


