from django import forms
from django import forms
from django.contrib.auth.forms import AuthenticationForm

class RegistroForm(forms.Form):
    cedula = forms.CharField(max_length=10)
    nombre_completo = forms.CharField(max_length=255)
    email = forms.EmailField()
    contraseña = forms.CharField(widget=forms.PasswordInput)
    telefono = forms.CharField(max_length=15)


class CustomLoginForm(AuthenticationForm):
    email = forms.EmailField(
        label="Email",  # Etiqueta personalizada para el campo de email
        widget=forms.TextInput(attrs={'class': 'form-control'}),  # Clase CSS para el campo
    )
    contraseña = forms.CharField(
        label="Contraseña",  # Etiqueta personalizada para el campo de contraseña
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),  # Clase CSS para el campo
    )

