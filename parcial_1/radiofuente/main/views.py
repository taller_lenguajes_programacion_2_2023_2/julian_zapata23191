from django.shortcuts import render, redirect
from .forms import RegistroForm
from django.contrib.auth.models import User
from .models import Usuario
from .forms import CustomLoginForm
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from .forms import CustomLoginForm  # Asegúrate de importar tu formulario personalizado


def registrar_usuario(request):
    if request.method == 'POST':
        form = RegistroForm(request.POST)
        if form.is_valid():
            # Crea un nuevo usuario de autenticación de Django
            usuario_auth = User.objects.create_user(
                username=form.cleaned_data['email'],
                password=form.cleaned_data['contraseña'],
                email=form.cleaned_data['email'],
            )
            usuario_auth.save()

            # Redirige al usuario a una página de éxito o a donde lo necesites
            print("Usuario registrado exitosamente")
    else:
        form = RegistroForm()

    return render(request, 'main/registro.html', {'form': form})



def iniciar_sesion(request):
    if request.method == 'POST':
        form = CustomLoginForm(request, data=request.POST)
        if form.is_valid():
            # Autentica al usuario utilizando las credenciales proporcionadas
            usuario = authenticate(
                request,
                username=form.cleaned_data['username'],  # Usar el campo de nombre de usuario
                password=form.cleaned_data['password']
            )
            if usuario is not None:
                # Inicia sesión al usuario autenticado
                login(request, usuario)
                # Redirige al usuario a una página de éxito o a donde lo necesites
                return redirect('main/pagina_principal.html')
    else:
        form = CustomLoginForm()

    return render(request, 'main/registro.html', {'form': form})
