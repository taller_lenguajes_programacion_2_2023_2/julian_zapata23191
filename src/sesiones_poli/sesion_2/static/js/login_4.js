let signup = document.querySelector(".signup");
let login = document.querySelector(".login");
let slider = document.querySelector(".slider");
let formSection = document.querySelector(".form-section");
const expresionRegularCorreo = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

signup.addEventListener("click", () => {
	slider.classList.add("moveslider");
	formSection.classList.add("form-section-move")
});

login.addEventListener("click", () => {
	slider.classList.remove("moveslider");
	formSection.classList.remove("form-section-move");
});

function validateLogin() {
	let email = document.getElementById("loginEmail").value;
	let password = document.getElementById("loginEmail").value;

	email = email.trim();
	password = password.trim();

	if(email != 'julian@gmail.com' && password != '1234'){
		alert("Ingrese credenciales válidas");
		email.value = "";
    	password.value = "";

		window.location.reload();
		return;
	}
	window.location.href = "C:/Users/Julian/Desktop/TALLER 2 REPO/julian_zapata23191/src/sesiones_poli/sesiones_5y6/index.html";
}

function validateSignUp() {

	let nameRegister = document.getElementById("nameRegister").value;
	let emailRegister = document.getElementById("emailRegister").value;
	let passwordRegister = document.getElementById("passwordRegister").value;
	let confirmPasswordRegister = document.getElementById("confirmPasswordRegister").value;

	nameRegister = nameRegister.trim();
	emailRegister = emailRegister.trim();
	passwordRegister = passwordRegister.trim();
	confirmPasswordRegister = confirmPasswordRegister.trim();

	// VALIDA si todos los campos se digitaron
	if(!nameRegister || !emailRegister || !passwordRegister || !confirmPasswordRegister){
		alert("Ingrese todos los campos");
		return;
	}

	// VALIDA si el correo está bien escrito
	let correoValido = validarCorreo(emailRegister);
	if (!correoValido) {
		alert("El correo electrónico no es válido.");
		return;
	}

	// VALIDA si las dos contraseñas coinciden
	if(passwordRegister != confirmPasswordRegister){
		alert("Los correos ingresados NO coinciden.");
		return;
	}

		alert("Usuario registrado correctamente");
		window.location.reload();
}

function validarCorreo(correo) {
    return expresionRegularCorreo.test(correo);
}


